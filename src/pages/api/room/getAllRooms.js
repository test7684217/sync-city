import prisma from "../../../lib/prisma";

export default async function handler(req, res) {
  try {
    const rooms = await prisma.room.findMany();

    return res.status(201).json({ msg: "Room fetched", status: true, rooms });
  } catch (error) {
    console.error("Room fetch error:", error);
    return res.status(500).json({
      msg: "Internal server error",
      status: false,
      error: error.message,
    });
  }
}
